﻿using System.Collections.Generic;

namespace TheApp
{
    public class Street
    {
        public int StartIntNum;
        public int FinalIntNum;
        public string StreetName;
        public int Time;
        public int Score;
    }

    public class CarRoute
    {
        public int StreetCount;
        public List<string> StreetNames;
    }

    public class DataSet
    {
        public DataSet()
        {
            Streets = new Dictionary<string, Street>();
            CarRoutes = new List<CarRoute>();
        }
        public int Sec;
        public int IntCount;
        public int StreetCount;
        public int CarCount;
        public int Points;
        public Dictionary<string, Street> Streets;
        public List<CarRoute> CarRoutes;
    }
}
