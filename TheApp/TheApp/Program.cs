using System;
using System.Collections.Generic;

namespace TheApp
{
    class Program
    {
        public static List<string> DataFilesList = new List<string>
        {
            "a.txt",
            "b.txt",
            "c.txt",
            "d.txt",
            "e.txt",
            "f.txt"
        };

        static void Main()
        {
            foreach (var fileName in DataFilesList)
            {
                var dataSet = InputOutput.ReadData($"data/{fileName}"); 
                var resultSet = new InputOutput.ResultSet();

                for(int i = 0; i < dataSet.IntCount; i++)
                {
                    resultSet.Intersections.Add(new InputOutput.Intersection
                    {
                        Id = i,
                        GreenStreets = new Dictionary<string, InputOutput.StreetGreen>(),
                        ScoreFinal = 0,
                        ScoreInitial = 0
                    });
                }

                foreach (KeyValuePair<string, Street> str in dataSet.Streets)
                {
                    resultSet.Intersections[str.Value.FinalIntNum].ScoreFinal++;
                    resultSet.Intersections[str.Value.StartIntNum].ScoreInitial++;
                }

                 foreach (KeyValuePair<string, Street> str in dataSet.Streets)
                {
                    if (str.Value.Score > 0)
                    {
                        Random rnd = new Random();
                        Random rnd2 = new Random();
                        resultSet.Intersections[str.Value.FinalIntNum].GreenStreets[str.Value.StreetName] = new InputOutput.StreetGreen
                        {
                            Title = str.Value.StreetName,
                            //Sec = (int)Math.Ceiling((decimal)str.Value.Score / (decimal)(dataSet.Sec) * (decimal)rnd.Next(1000))
                            Sec = rnd.Next(100, 500)
                        };

                        /*if(str.Value.Score > dataSet.Sec)
                        {
                            resultSet.Intersections[str.Value.FinalIntNum].GreenStreets[str.Value.StreetName] = new InputOutput.StreetGreen
                            {
                                Title = str.Value.StreetName,
                                Sec = (int)Math.Ceiling((decimal)dataSet.Sec / (decimal)(resultSet.Intersections[str.Value.FinalIntNum].ScoreFinal))
                            };
                        }*/
                    }
                }

                List<InputOutput.Intersection> intersectionsFixed = new List<InputOutput.Intersection>();

                foreach (var intersection in resultSet.Intersections)
                {
                    if (intersection.GreenStreets.Count > 0)
                    {
                        foreach (var street in intersection.GreenStreets.Values)
                        {
                            intersection.GreenStreetsList.Add(street);
                        }
                        intersection.GreenStreetsList.Sort((x, y) => x.Sec.CompareTo(y.Sec));
                        intersectionsFixed.Add(intersection);
                    }
                }

                resultSet.Intersections = intersectionsFixed;

                Console.WriteLine(fileName);

                InputOutput.OutputData($"{fileName}-result.out", resultSet);
            }
        }
    }
}
