﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.ExceptionServices;

namespace TheApp
{
    public class InputOutput
    {
        public static DataSet ReadData(string filename)
        {
            var dataSet = new DataSet();

            using (var sr = new StreamReader(filename))
            {
                var info = sr.ReadLine().Trim().Split(' ');

                dataSet.Sec = int.Parse(info[0]);
                dataSet.IntCount = int.Parse(info[1]);
                dataSet.StreetCount = int.Parse(info[2]);
                dataSet.CarCount = int.Parse(info[3]);
                dataSet.Points = int.Parse(info[4]);

                for (var i = 0; i < dataSet.StreetCount; i++)
                {
                    // ReSharper disable once PossibleNullReferenceException

                    var streetInfo = sr.ReadLine().Trim().Split(' ');

                    Street street = new Street()
                    {
                        StartIntNum = int.Parse(streetInfo[0]),
                        FinalIntNum = int.Parse(streetInfo[1]),
                        StreetName = streetInfo[2],
                        Time = int.Parse(streetInfo[3]),
                        Score = 0
                    };

                    dataSet.Streets[street.StreetName] = street;
                }

                for (var i = 0; i < dataSet.CarCount; i++)
                {
                    // ReSharper disable once PossibleNullReferenceException

                    var carRouteInfo = sr.ReadLine().Trim().Split(' ');

                    CarRoute carRoute = new CarRoute()
                    {
                        StreetCount = int.Parse(carRouteInfo[0]),
                        StreetNames = new List<string>()
                    };

                    for (var j = 0; j < carRoute.StreetCount; j++)
                    {
                        carRoute.StreetNames.Add(carRouteInfo[j + 1]);
                        dataSet.Streets[carRouteInfo[j + 1]].Score += 1;
                    }

                    dataSet.CarRoutes.Add(carRoute);
                }

                return dataSet;
            }
        }

        //public class Slide
        //{
        //    public Picture Pic1;
        //    public Picture Pic2;
        //    public HashSet<string> Tags;
        //    public bool Used;

        //    public Slide(Picture pic1)
        //    {
        //        Pic1 = pic1;
        //       Tags = new HashSet<string>();
        //       Tags.UnionWith(pic1.Tags);
        //    }

        //    public override string ToString()
        //    {
        //        var result = string.Empty;
        //        if (Pic1.Orientation == "H")
        //        {

        //            return Pic1.Id.ToString();
        //        }

        //        return $"{Pic1.Id} {Pic2.Id}";
        //    }

        //    public int GetScore(Slide other)
        //    {
        //        var intersection = this.Tags.Intersect(other.Tags);
        //        var count = intersection.Count();
        //        var left = this.Tags.Count - count;
        //        var right = other.Tags.Count - count;
        //        return Math.Min(Math.Min(count, left), right);

        //    }
        //}

        public class StreetGreen
        {
            public string Title;
            public int Sec;
        }

        public class Intersection
        {
            public Intersection()
            {
                GreenStreetsList = new List<StreetGreen>();
            }
            public Dictionary<string, StreetGreen> GreenStreets;
            public List<StreetGreen> GreenStreetsList;
            public int Id;
            public int ScoreInitial;
            public int ScoreFinal;
        }

        public class ResultSet
        {
            public ResultSet()
            {
                Intersections = new List<Intersection>();
            }

            public List<Intersection> Intersections;
        }

        public static void OutputData(string filename, ResultSet results)
        {
            using (var outputFile = new StreamWriter(filename))
            {
                outputFile.WriteLine(results.Intersections.Count);
                /*
                foreach (var intersection in results.Intersections)
                {
                    outputFile.WriteLine(intersection.Id);
                    outputFile.WriteLine(intersection.GreenStreets.Count);

                    foreach (KeyValuePair<string, StreetGreen> greenStreet in intersection.GreenStreets)
                    {
                        outputFile.WriteLine($"{greenStreet.Value.Title} {greenStreet.Value.Sec}");
                    }
                }*/
                foreach (var intersection in results.Intersections)
                {
                    outputFile.WriteLine(intersection.Id);
                    outputFile.WriteLine(intersection.GreenStreetsList.Count);

                    foreach (var greenStreet in intersection.GreenStreetsList)
                    {
                        outputFile.WriteLine($"{greenStreet.Title} {greenStreet.Sec}");
                    }
                }
            }
        }

    }
}
